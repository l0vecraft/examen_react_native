'use stric'

let hotelModel = require('../models/hotel');
let fs = require('fs');
let path = require('path');

var hotelController = {
    home: (req,res)=>{
        return res.status(200).send({
            message:'holi'
        })
    },

    add:(req,res)=>{
        let params = req.body;
        let newHotel = new hotelModel();

        newHotel.nombre = params.nombre;
        newHotel.descripcion = params.descripcion;
        newHotel.score = params.score;
        newHotel.images = null;
        newHotel.price = params.price;

        hotelModel.save((error,hotel)=>{
            if(error) return res.status(500).send({error:'Error en el servidor'});
            if(!hotel) return res.status(400).send({error:'No se pudo crear el hotel'});
            return res.status(200).send({hotel});
        });
    },
    listHotel:(req,res)=>{
        hotelModel.find({}).exec((err,hotels)=>{
            if(err) return res.status(500).send({error:'Error en el servidor'});
            if(!hotels) return res.status(404).send({error:'No se encontraron hoteles'});
            return res.status(200).send({hotels});
        });
    },
    getHotel:(req,res)=>{
        let id = req.params.id;
        if(id==null) return res.status(404).send({error:'Error, ID desconocido'});
        
        hotelModel.findById(id,(err,hotel)=>{
            if(err) return res.status(500).send({error:'Error en el servidor'});
            if(!hotel) return res.status(404).send({error:'No se encontro hotel'});
            return res.status(200).send({hotel});
        });
    },
    update:(req,res)=>{
        let id = req.params.id;
        let update = req.body;
        hotelModel.findByIdAndUpdate(id,update,{new:true},(err,hotel)=>{
            if(err) return res.status(500).send({error:'Error en el servidor'});
            if(!hotel) return res.status(404).send({error:'No se pudo actualizar el hotel'});
            return res.status(200).send({hotel});
        });
    },
    delete:(req,res)=>{
        let id = req.params.id;
        hotelModel.findByIdAndDelete(id,(err,hotel)=>{
            if(err) return res.status(500).send({error:'Error en el servidor'});
            if(!hotel) return res.status(404).send({error:'No se encontro el hotel a borrar'});
            return res.status(200).send({msg:'Hotel borrado con exito'});
        });
    },
        uploadImages:(req,res)=>{
            let id = req.params.id;
            let fileName ='Imagen no subida';

            if(req.files){
                let filePath = req.files.path;
                let fileSplit = filePath.fileSplit('/');
                let fileName = fileSplit[1];
                let nameSplit = fileName.split('.');
                let ext = nameSplit[1];

                if(ext=='jpeg' | ext == 'jpg' | ext=='png'){
                    hotelModel.findByIdAndUpdate(id,{images:fileName},(err,hotel)=>{
                        if(err) return res.status(500).send({error:'Error en el servidor'});
                        if(!hotel) return res.status(404).send({error:'No se encontro el hotel'});
                        return res.status(200).send({hotel});
                    });
                }else{
                    fs.unlink(filePath,(err)=>{
                        return res.status(403).send({message:'Error , formato incorrecto'})
                    })
                }
            }else{
                return res.status(404).send({message:'Error en el cuerpo de la peticion'});
            }
        },
        
        getImages:(req,res)=>{
            var file = req.params.image;
            var path_file = './uploads/'+file;

            fs.exists(path_file,(exists)=>{
                if(exists){
                    return res.sendFile(path.resolve(path_file));
                }else{
                    return res.status(200).send({message:'No existe la imagen'});
                }
            });
        }
}

module.exports = hotelController;