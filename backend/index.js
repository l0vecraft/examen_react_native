'use strict'

let mongoose = require('mongoose');
let app = require('./app');
let port = 3990;

mongoose.Promise=global.Promise;
mongoose.connect('mongodb://localhost:27017/hotel')
        .then(()=>{
            console.log('Conexion exitosa a la db');
            app.listen(port,()=>{
                console.log('Servidor corriendo en http://localhost:'+port);
            })
        }
        );