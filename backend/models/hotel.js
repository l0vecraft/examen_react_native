'use strict'

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let hotelModel = Schema({
    nombre:String,
    descripcion:String,
    score:Number,
    images: [],
    price: Number
});

module.exports = mongoose.model('Hotel',hotelModel);