'use stric'

let express = require('express');
let hotelController = require('../controller/hotelController');

let router = express.Router();
let multipart = require('connect-multiparty');
let multipartMiddle = multipart({uploadDir:'./uploads'});

//prueba
router.get('/home',hotelController.home);
//agregar hotel
router.post('/add',hotelController.add);
//listar hoteles
router.get('/hotels',hotelController.listHotel);
//actualizar hoteles
router.put('/hotel/:id',hotelController.update);
//eliminar hoteles
router.post('/hotel/:id',hotelController.delete);
//subir imagenes
router.post('/Uploadimages/:id',multipartMiddle,hotelController.uploadImages)
//obtener imagenes
router.get('/image/:image',hotelController.getImages);

module.exports=router;